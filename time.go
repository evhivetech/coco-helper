package cocohelper

import (
	"errors"
	"math/rand"
	"time"
)

type TimeHandler interface {
	BeginningOfMonth() time.Time
	EndOfMonth() time.Time
}

// Now now struct
type Now struct {
	t time.Time
}

// New initialize Now with time
func New(t time.Time) *Now {
	return &Now{t}
}

// BeginningOfMonth beginning of month
func (now *Now) BeginningOfMonth() time.Time {
	y, m, _ := now.t.Date()
	return time.Date(y, m, 1, 0, 0, 0, 0, now.t.Location())
}

// EndOfMonth end of month
func (now *Now) EndOfMonth() time.Time {
	return now.BeginningOfMonth().AddDate(0, 1, 0).Add(-time.Nanosecond)
}

// GetNowTimeJakarta - Golang unzip from timezone fail
func GetNowTimeJakarta() time.Time {
	loc, err := GetJakartaLoc()
	if err != nil {
		return time.Now().Add(time.Hour * time.Duration(7)) // Add 7 hours from local time
	}

	t := time.Now().In(loc)
	return t
}

// GetJakartaLoc - Time Location GMT
func GetJakartaLoc() (loc *time.Location, err error) {
	// 1st Try Indonesia
	loc, err = time.LoadLocation("Asia/Jakarta")
	if err != nil {
		// 2nd Try Thailand
		loc, err = time.LoadLocation("Asia/Bangkok")
		if err != nil {
			// 3rd Try Vietnam
			loc, err = time.LoadLocation("Asia/Saigon")
		}
	}

	return loc, err
}

// AddTimeWithRandomSecond - Add with Random Second
func AddTimeWithRandomSecond(timestamp time.Time) time.Time {
	rand.Seed(time.Now().UnixNano())
	incr := rand.Intn(45)

	return timestamp.Add(time.Second * time.Duration(incr))
}

func ConvertStringToTime(stringTime string) (time.Time, error) {
	var t time.Time
	var err error
	timeFormats := []string{
		time.RFC3339Nano,
		time.RFC3339,
		"2006-01-02",
		"2006-01-02T15:04:05Z",
		"2006-01-02T15:04:05Z0700",
		"2006-01-02T15:04:05Z07:00",
		"2006-01-02T15:04:05-0700",
		"2006-01-02T15:04:05-07",
		"2006-01-02T15:04:05-07:00",
	}

	for _, timeFormat := range timeFormats {
		t, err = time.Parse(timeFormat, stringTime)
		if err == nil {
			return t, err
		}
	}

	return time.Time{}, errors.New("Unsupported time or datetime format")
}

package cocohelper

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
)

//COCOJSONFormatter - custom log format for devops
type COCOJSONFormatter struct {
	// DataKey allows users to put all the log entry parameters into a nested dictionary at a given key.
	DataKey string
}

func (f *COCOJSONFormatter) Format(entry *log.Entry) ([]byte, error) {
	data := make(log.Fields, len(entry.Data)+3)
	for k, v := range entry.Data {
		switch v := v.(type) {
		case error:
			data[k] = v.Error()
		default:
			data[k] = v
		}
	}

	if f.DataKey != "" {
		newData := make(log.Fields, 4)
		newData[f.DataKey] = data
		data = newData
	}

	if data["thread"] == nil {
		data["thread"] = 00000
	}
	if data["session"] == nil {
		data["session"] = 00000
	}

	//set time in key
	data["dt"] = entry.Time
	data["message"] = entry.Message
	data["loglevel"] = strings.ToUpper(entry.Level.String())
	data["process"] = os.Getpid()

	serialized, err := json.Marshal(data)
	if err != nil {
		return nil, fmt.Errorf("Failed to marshal fields to JSON, %v", err)
	}

	return append(serialized, '\n'), nil
}

//GormLogger - custom gorm log format for devops
type GormLogger struct{}

func (*GormLogger) Print(v ...interface{}) {
	if v[0] == "sql" {
		log.WithFields(log.Fields{"module": "gorm"}).Debug(v[3])
	}
	if v[0] == "log" {
		log.WithFields(log.Fields{"module": "gorm"}).Debug(v[2])
	}
}

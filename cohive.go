package cocohelper

import "time"

// GetLeaseLength - Calculate Lease Length based start and end date
func GetLeaseLength(startDate, endDate time.Time) int {
	leaseLength := ((endDate.Year()*12 + int(endDate.Month())) - (startDate.Year()*12 + int(startDate.Month())))
	if startDate.Day() <= endDate.Day() {
		leaseLength = leaseLength + 1
	}

	return leaseLength
}

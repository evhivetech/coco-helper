package cocohelper

import (
	"math/rand"
	"time"
)

const (
	STRINGCHARSET = `abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~!@#$%^&*()_+=-`
)

var (
	seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))
)

func StringRandom(length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = STRINGCHARSET[seededRand.Intn(len(STRINGCHARSET))]
	}
	return string(b)
}

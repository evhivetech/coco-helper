package cocohelper

import (
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
)

func Sha256Encrypt(s string) string {
	h := sha256.New()
	h.Write([]byte(s))

	return hex.EncodeToString(h.Sum(nil))
}

func Base64EncodeURL(s string) string {
	data := []byte(s)
	encode := base64.URLEncoding.EncodeToString(data)

	return encode
}

func Base64DecodeURL(s string) string {
	data, err := base64.URLEncoding.DecodeString(s)
	if err != nil {
		fmt.Println("error when decode")
		log.Error(err)
		return ""
	}

	fmt.Println(string(data))
	return string(data)
}

func EncodeMemberID(ID int, timestamp time.Time) string {
	member := "&member=" + Base64EncodeURL("Cohive"+strconv.Itoa(ID))
	xToken := "&x-token=" + Sha256Encrypt(timestamp.String())

	return member + xToken
}
